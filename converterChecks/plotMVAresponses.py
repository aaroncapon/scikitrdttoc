import pickle
import pandas as pd
import warnings
from sklearn.ensemble import RandomForestClassifier
from sklearn.externals import joblib

from ROOT import *
from root_numpy import root2array

# All settings below must be customised.
if __name__=='__main__':

    ##------------- Settings ----------------------
    # Path to original classifier (saved as pkl)
    classifier_file = '<INSERT PATH HERE>/classifier.pkl'

    # Name of local TTree that will be used to check correctness of C style
    # classifier against the python version
    path_to_file = "<INSERT PATH HERE>/dataset.root"
    tree_name    = ""
    
    # Name of output file to store class plots
    output_file = "outputsFromPython.root"
    # List of all possible output classes (same order as during training)
    # Also used as names for saved histograms
    num_classes = 5
    class_names =["histElecs", "histMuons", "histPions", "histKaons", "histProtons"]

    # Histograms specifications 
    # One histograms per class will be produced with the probability of each
    # track being from said class being plotted.
    num_bins = 74
    hist_min = 0
    hist_max = 1

    # List of features used by the classifier
    num_features = 25
    input_features =[
            "TPCNcls", "TPCsignal", "ped", "Pt", "Px", "Py", "Pz", "nSigmaTOFPi",
            "nSigmaTOFK", "nSigmaTOFP", "nSigmaTOFe", "nSigmaTPCPi", "nSigmaTPCK",
            "nSigmaTPCP", "nSigmaTPCe", "tbeta", "cov2", "cov5", "cov9",
            "cov13", "cov14", "cov17", "cov18", "cov19", "cov20"]

    ##---------------------------------------------

    # Read in classifier. Ignore version warnings...
    # TODO: implement system to catch when different versions will actually
    # impact classifier performance
    with warnings.catch_warnings():
        warnings.simplefilter("ignore", category = UserWarning)
        classifier = joblib.load(classifier_file)

    print("Reading in data......")
    data = pd.DataFrame(root2array(path_to_file, treename=tree_name, branches=input_features))

    # Create histograms. One for each class probability
    list_class_outputs = []
    for i in range(0, num_classes):
        hist = TH1D(class_names[i], class_names[i], num_bins, hist_min, hist_max)
        list_class_outputs.append(hist)

    # Get predictions for each track and fill histograms
    response = classifier.predict_proba(data)
    print(response)

    # for i in range(0, len(response)):
    for track in range(0, len(response)):
        # Classifier probabilities for each class
        for i in range(0, num_classes):
           list_class_outputs[i].Fill(response[track][i])

    # Save histogram to ROOT file
    out_file = TFile.Open(output_file, "RECREATE")
    try:
        out_file
    except NameError:
        print("Output file could not be opened. Plots not saved!")
    else:
        out_file.cd()
        for i in range(0, num_classes):
            list_class_outputs[i].Write()

        out_file.Close()
