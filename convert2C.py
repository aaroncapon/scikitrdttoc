# Script to convert a scikit Random Tree Classifier, in .pkl form, to
# C style code.
# The underlying conversion for each Decision Tree is adapted from:
# github.com/papkov/DecisionTreeToCpp
#
# The output file in C can be very large if formatted in the standard way.  To
# reduce the file size, pass "0" to the script and all tabs/newlines will be
# omitted.
# If readability of the individual decisions being made by the trees, omit any
# argument and standard C formatting will be applied.


from sklearn.ensemble import RandomForestClassifier
from sklearn.externals import joblib
import warnings
import sys
import os.path
from sklearn.tree import _tree

# Write small intro into C header file.
#   Prints list of input features and output class names as header comment
#   Defines function and inputs
def file_intro(feature_names, num_classes, function_name="decision_tree"):

    code = ''
    feature_string = ""
    for i in range(0, len(feature_names)):
        feature_string += 'feature_vector[{0}] - {1}\n'.format(i, feature_names[i])
    classes_string = ""
    for i in range(0, len(class_names)):
        classes_string += '{0} - {1}\n'.format(i, class_names[i])

    preamble = """/*
This function takes an array of feature values,
{0}
also passed to it is an array which will be filled with the class probabilities:
{1}
*/
""".format(feature_string, classes_string)

    code += '{0}\n\n'.format(preamble)
    code += 'void {0}(const float (&feature_vector)[{1}], double (&classes)[{2}]){{\n\n'.format(function_name, len(feature_names), num_classes)

    # code += '  double singleTreeOutput[{0}] = {0};\n'
    return code

# Obtain the C code for the Decision trees
def get_code(tree, feature_names, readable_code):

    # Get IDs of child nodes
    left  = tree.tree_.children_left
    right = tree.tree_.children_right
    # Threshold for feature used to split current node
    threshold = tree.tree_.threshold
    features = [feature_names[i] for i in tree.tree_.feature]
    value = tree.tree_.value

    # Function to recurse down the decision tree with newlines and spaces used.
    def recurse_spaces(left, right, threshold, features, node, tabs):
        code = ''
        # Check that current node is not leaf node
        if tree.tree_.feature[node] != _tree.TREE_UNDEFINED:
            code += '{0}if(feature_vector[{1}] <= {2}){{\n'.format(tabs * '  ', feature_names.index(features[node]), threshold[node])
            tabs += 1

            # Recursively traverse left nodes until final leaf
            if left[node] != _tree.TREE_LEAF:
                code += recurse_spaces(left, right, threshold, features, left[node], tabs)

            tabs -= 1
            code += '{0}}}\n{1}else{{\n'.format(tabs * '  ', tabs * '  ')
            tabs += 1

            # Recusively traverse right nodes until final leaf
            if right[node] != _tree.TREE_LEAF:
                code += recurse_spaces(left, right, threshold, features, right[node], tabs)

            tabs -= 1
            code += '\n{0}}}'.format(tabs * '  ')

        # Calculate probabilities for leaf node
        else:
            leaf_entries = value[node].sum()
            probas = [ x/leaf_entries for x in value[node]]
            classes = len(probas[0])

            code += '{0}double tempArray[{1}] = {{'.format(tabs*'  ', classes)
            for i in range(0, classes):
                code += '{0}'.format(probas[0][i])
                if i < classes-1:
                    code += ','

            code += '};\n'
            code += '{0}for(int i = 0; i < {1}; ++i){{classes[i] += tempArray[i];}}'.format(tabs*'  ', classes)

        return code

    # Function to recurse down the decision tree with nearly all newlines and spaces
    # removed. One newline per feature check is implemented as this makes the
    # file readable and has negligible effect on the size when compared to a
    # file that has no newlines.
    def recurse(left, right, threshold, features, node):
        code = ''
        # Check that current node is not leaf node
        if threshold[node] != _tree.TREE_UNDEFINED:
            code += 'if(feature_vector[{0}] <= {1}){{'.format(feature_names.index(features[node]), threshold[node])

            # Recursively traverse left nodes until final leaf
            if left[node] != _tree.TREE_LEAF:
                code += recurse(left, right, threshold, features, left[node])

            code += '}\nelse{'
            # Recursively traverse right nodes until final leaf
            if right[node] != _tree.TREE_LEAF:
                code += recurse(left, right, threshold, features, right[node])

            code += '}'
        else:
            leaf_entries = value[node].sum()
            probas = [ x/ leaf_entries for x in value[node]]
            classes = len(probas[0])

            code += 'float tempArray[{0}] = {{'.format(classes)
            for i in range(0, classes):
                code += '{0}'.format(probas[0][i])
                if i < classes-1:
                    code += ','

            code += '};'
            code += '{0}for(int i = 0; i < {1}; ++i){{classes[i] += tempArray[i];}} '.format(' ', classes)

        return code

    if readable_code == True:
        code = '{0}\n'.format(recurse_spaces(left, right, threshold, features, 0, 1))
    else:
        code = '{0}\n'.format(recurse(left, right, threshold, features, 0))

    return code


def save_code(code, function_name="decision_tree"):

    with open(function_name + '.h', "w") as f:
        f.write(code)
        print("\nC style classifier was written to: {0}".format(function_name + '.h'))

    return 0

if __name__=='__main__':

    # Read in classifier
    # Suppress warning from mismatch in scikit versions
    if len(sys.argv) < 2 or (str(sys.argv[1]).find(".pkl") == -1):
        print("#####  ERROR  #####")
        print("Must pass classifier as first argument!")
        print("Example usage: python <CLASSIFIER.pkl> <CLASSIFIER_DETAILS.txt> <optional arg>")
        exit()
    else:
        with warnings.catch_warnings():
            warnings.simplefilter("ignore", category = UserWarning)
            classifier = joblib.load(sys.argv[1])

    # If second argument missing, write readable code
    readable_code = True
    if len(sys.argv) < 3:
        print("----    No formatting argument passed   ------")
        print("-> Standard formatting will be used")
    else:
        # Flag to switch on readable formatting
        if int(sys.argv[2]) == 0:
            readable_code = False


    # Get lists for input features and output class
    # If not stored within the pkl file, try to import from .txt file
    # Read in text file containing classifier details
    if hasattr(classifier, 'feature_names') and hasattr(classifier, 'class_names'):
        feature_names = classifier.feature_names
        class_name    = classifier.class_names
    elif os.path.isfile('classifier_details.txt'):
        text_file = open('classifier_details.txt', 'rt')
        feature_names = text_file.readline().split()
        class_names   = text_file.readline().split()
        text_file.close()
    else:
        print("#### ERROR ####")
        print("Classifier features and classes could not be retrieved from pkl")
        print("\'classifier_details.txt\' does not exist.")
        print("#### ABORTING ####")
        exit()


    num_trees   = len(classifier.estimators_)
    num_classes = len(class_names)

    # Output basic details
    print("##### Classifier details  #####")
    print("Number of trees: {0}".format(num_trees))
    print("Input features: {0}".format(feature_names))
    print("Output classes: {0}".format(class_names))

    code = file_intro(feature_names, num_classes)

    # Loop each individual decision tree and append to code string
    for i in range(0, num_trees):
        code += '\n\t// Tree: {0}\n'.format(i)
        single_tree = classifier.estimators_[i]
        code += get_code(single_tree, feature_names, readable_code)

    ## Compute "probability" for each class using votes from each tree
    code += """\t// Divide the entries by the number of trees
\t// to get the average probability for each class\n"""
    for i in range(0, num_classes):
        code += '\t\tclasses[{0}] /= {1};'.format(i, num_trees)

    code += '}'

    if not readable_code:
        code = code.replace("feature_vector", "f").replace("classes", "c").replace("tempArray", "t")

    save_code(code)

    exit()
