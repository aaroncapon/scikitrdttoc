# Scikitlearn Random Forest to C Converter


## Running the Conversion Script  
Run `python convert2C.py <CLASSIFIER.pkl> <opt_arg>` to obtain C code.

**<CLASSIFIER.pkl>**: the scikitlearn Random Forest classifier  
**<opt_arg>**: set to 1 for readable C formatting, and set to 0 for "unreadable"
code which will greatly reduce file size by omitting most tabs and spaces.

The script will try to get the names of the input features and target classes
directly from the .pkl. If they were not added to .pkl during training, the
script will search for **classifier\_features.txt** in the current directory.
**classifier\_details.txt** should contain two comma separated lists. The first
line must contain the names of the input features, and the second line the names
of the output features. An example of such a file can be found in this repository.  

## Check Classifier Conversion  
To ensure that the conversion was correctly implemented, three extra scripts
have been included. Each script contains a "Settings" section below the primary
function defintion. One must customise the paths to point to your data sets and
classifiers, as well alter the list of target classes, input features etc.  
**plotMVAresponses.C and plotMVAresponses.C**  
  Simple scripts to plot the probability of each track to be a certain particle
  species (class).  
**compareResults.C**  
  Takes the ouputs from the above scripts and compares the results. Results are
  shown as ratio plots for each target class, and a bin-by-bin check it also
  performed with any mismatches being printed.  
  
  
  
  
  
## Created and tested using:  
G++ 5.4.0  
Python 2.7.12  
sklearn 0.20.2  
numpy 1.15.1  
root\_numpy 4.8.0  
ROOT 6.13/01  

